# awesome-links
Personal collection of links worth to be shared in public as well.

## Transport & Geography

- [Berlin Brandenburg Public Transport Real Time Map](http://fahrinfo.vbb.de/bin/help.exe/dn?L=vs_mobilitymap&&tpl=fullmap&tabApp=show)

## Development

### Letsencrypt

- [gethttpsforfree.com](https://gethttpsforfree.com/) - Generate an letsencrypt certifikat using your own csr on a single website

### Benchmarking & Web Perfomance

- [wrk](https://github.com/wg/wrk) - http benchmarking tool (usually part of Linux distros)
- [perf.rocks - Web Performance Matters](http://perf.rocks/) - a nice curated collection of tools, blogposts, and other stuff around this topic
- [Perfomance Planet's Yearly Digest](http://calendar.perfplanet.com/2016/) - A collection of great blog posts and articles of the whole year (and year before, and ...)

### git

- Clients, Hosting and (Web-)Interfaces
  - [gitlab.com](https://gitlab.com) - Offers the same as github.com but also unlimited private organisations (called Groups), unlimited private repositories, CI/CD interface, docker registry, web based merge conflict solution and a Community Edition that could be installed on a server. It's based on ruby on rails. [Check all the features](https://about.gitlab.com/features/)
  - [Openhub](https://www.openhub.net/) - Open Source Project Hub
  - [Ungit](https://github.com/FredrikNoren/ungit) - Local git client that helps to deal with advanced git tasks. It is made in a very visual way and based on node.js 
- Insights
  - [Gittrends](http://gittrends.io/#/) - Insights regarding popularity of git projects
- Tools
  - [diff-so-fancy](https://github.com/so-fancy/diff-so-fancy) - Improve diff look
- Learning
  - [Short guide covering the main use cases](https://rogerdudler.github.io/git-guide/index.de.html) - in German only 
  - [Rypress Git Tutorial](http://rypress.com/tutorials/git/index) - Tutorial, explaining basics and advanced concepts.
- Concrete Use Cases
  - [Script to rewrite History](https://help.github.com/articles/changing-author-info/) with another name or email address

### Markdown

- [Markdown Syntax, github flavored](https://help.github.com/categories/writing-on-github/)
- [Automatic TOC generator](https://github.com/ekalinin/github-markdown-toc)
- [Markdown vs. Restructured text comparison table](http://hyperpolyglot.org/lightweight-markup)
- [MkDocs - NIce looking Project Documentation](http://www.mkdocs.org/) - Example: [Skipper](https://zalando.github.io/skipper/)

### Latex

- [Kile IDE](tecadmin.net/install-kile-ubuntu/)

### Continious Integration/Deployment

- [Travis CI](https://travis-ci.org/) - Free (for FLOSS) Continious Integration solution. Webbased.

### Static page generators

#### Overview

- [List of static page generators](https://staticsitegenerators.net/)
  - Jekyll
  - Sphinx
  - [nanoc](https://nanoc.ws/about/) - ruby based and used for building the gitlab documentation

#### Jekyll

- Commit _site directory or not? Well, it depends: [Good Answer](https://stackoverflow.com/a/31871892/3752157)

#### Text and Sourcecode editors

-[Atom](https://atom.io/) - good git integration!

#### HTML Editors

- [Simple Online HTML Editor](https://html-online.com/editor/) that allows to edit either source or WYSIWYG

## UX and Design

- [Large set of free emoticons](http://emojione.com/) that even could be used for comercial purposes (read conditions first!).

## Data-Science

- [dataquest.io](https://www.dataquest.io/) - Online courses for data science. At least the first 16 are for free!
- [d3.js](https://d3.js) - Make graphs available in web docs

## Linux

### Ubuntu

- [Remove unused kernel packages from system](http://askubuntu.com/a/259092/379395)
- [Recover boot menue in case the bootloader (grub2) was destroyed](https://forum.ubuntuusers.de/topic/grub-2-wiederherstellen/#post-2163906)

## Communication

- [Mattermost](https://about.mattermost.com/) - A Slack Alternative
- [Sieve Filter Link Collection](https://gitlab.com/jukey/awesome-links) - An awesome collection of links related to Sieve Filters
- [Backup IMAP Mail Accounts](https://github.com/RaymiiOrg/NoPriv#usage)

## Working Culture

- [remote.co](https://remote.co/) - Portal for companies that foster remote working
- [Blog article containing tons of remote working Job Board links](http://blog.trello.com/how-to-find-and-land-your-next-remote-job)

## Open Street Map (OSM)

- [Username based statistics for OSM contribution](https://hdyc.neis-one.org/)

## ebooks

- [MobileRead Forum](https://www.mobileread.com/) - Free (old) ebooks without

## Music Streaming

- [Playlist Manager for Spotify](http://playlist-manager.com/) - ([sources](https://github.com/eduardolima93/playlist-manager)) - Provides an easy to use interface to manage Playlists and Songs (Works in Chrome only currently)

## Tools

 - [draw.io](https://github.com/jgraph/draw.io) - The web based open source 'visio'
 
## Mobile

### iOS

 - [A collaborative list of iOS open source apps](https://github.com/dkhamsing/open-source-ios-apps#github)

# Books

- [Reinvent Orgs](http://www.reinventingorganizations.com/)